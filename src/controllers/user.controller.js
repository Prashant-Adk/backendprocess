import { asyncHandler } from "../utils/asyncHandler.js";
import { ApiError } from "../utils/ApiError.js";
import { User } from "../models/user.model.js";
import { uploadOnCloudnary } from "../utils/cloudinary.js";
import { ApiResponse } from "../utils/ApiResponse.js";
import jwt from "jsonwebtoken"



const generateAccessAndRefreshTokens = async (userId) => {
    try {
        const user = await User.findById(userId)
        const accessToken = user.generateAccessToken()
        const refreshToken = user.generateRefreshToken()

        user.refreshToken = refreshToken
        user.accessToken = accessToken
        // console.log(`user.refreshToken === ${user.refreshToken} ==\n this is access token ==  ${user.accessToken} \n user id == ${user._id}`)
        await user.save({ validateBeforeSave: false })
        console.log(`\n return access token = ${accessToken} \n return refresh token = ${refreshToken}`)
        return {
            accessToken,
            refreshToken
        }


    } catch (error) {
        throw new ApiError("500", "token generating went wrong")
    }

}
const updateImages = asyncHandler(async (req, res) => {
    const avatarLocalPath = req.files.avatar[0]?.path
    let coverImageLocalPath;
    if (Array.isArray(req.files.coverImage) && req.files.coverImage.length > 0 && req.files) {
        coverImageLocalPath = req.files.coverImage[0]?.path
    }
    if (!avatarLocalPath) {
        throw new ApiError(400, "avatar image is required")
    }

    const avatar = await uploadOnCloudnary(avatarLocalPath)
    const coverImage = await uploadOnCloudnary(coverImageLocalPath)


    if (!avatar) {
        throw new ApiError(400, "Avatar file needed")
    }
    return {
        avatar,
        coverImage
    }
})

const registerUser = asyncHandler(async (req, res) => {
    /*
    -- get user detail from postman(frontend)
    -- validate email, username, password and all(!empty)
    -- check if user already exist in DB using email or username
    -- check for imagess and avatar
    -- upload to cloudnary
    -- create user object in DB
    -- remove password and refresh token from response
    -- check for creation of user
    -- return response
    */

    const { username, email, fullname, password } = req.body;
    // console.log("emaill == ", email, ` ${JSON.stringify(req.body)}`);

    // if(fullname === ""){
    //    throw new ApiError(400,"Full name required")
    // }
    if (
        [fullname, username, email, password].some(
            (field) => field?.trim() === ""
        )
    ) {
        throw new ApiError(400, "All Fields are required");
    }
    const existedUser = await User.findOne({
        $or: [
            { username },
            { email }
        ]
    })
    if (existedUser) {
        throw new ApiError(409, "user already exist")
    }

    try {
        // const{avatar, coverImage} = await updateImages(req, res)
        //this can be written as bellow :-
        const image = await updateImages(req, res)
        var avatar = image.avatar;
        var coverImage = image.coverImage;

        // if(req.files.coverImage && req.files) {
        //     coverImageLocalPath = req.files.coverImage?.path
        // }
        // if(req.files.coverImage.length > 0) {
        //     coverImageLocalPath = req.files.coverImage[0].path
        // }
        // console.log(req.files)
        // console.log(req.files)
        if(avatar && coverImage) {
            console.log(`avatar image and coverImage found`)
        }
        else
            console.log(`avatar image and coverImage not found`)
    } catch (error) {
        return error
    }
    
    const user = await User.create({
        fullname,
        avatar: avatar.url,
        coverImage: coverImage?.url || "",
        email,
        password,
        username: username.toLowerCase()
    })
    const createdUser = await User.findById(user._id).select(
        "-password -refreshToken"
    )
    if (!createdUser) {
        throw new ApiError(500, "error while registering the user..... user not created")
    }

    return res.status(201)
        // .json({
        //     "avatar": avatar.public_id, 
        //     "coverImage": coverImage.public_id
        // })
        .json (new ApiResponse(200, createdUser, "User Registered Sucessfully"))
    // )


});

const loginUser = asyncHandler(async (req, res) => {
    //login with email and password(req.body)
    //validate the email and password from database using jwt token that is mached from user/register.
    //if validated return true else print("email password not matched")
    //generate access and refresh token
    //send token in cookies


    const { email, username, password } = req.body

    // console.log(`this is email and username == ${email, username}`)
    if (!username && !email) {
        throw new ApiError("400", "username or email is required")
    }

    const user = await User.findOne({
        $or: [
            { username },
            { email }
        ]
    })
    if (!user) {
        throw new ApiError("404", "user doesn't exist")
    }

    const isPasswordValid = await user.isPasswordCorrect(password)
    console.log(` is password valid == ${isPasswordValid}`)

    if (!isPasswordValid) {
        throw new ApiError("401", "Valid Password is required")
    }
    // console.log("user id = = ", user._id)
    const { accessToken, refreshToken } = await generateAccessAndRefreshTokens(user._id)
    // console.log( "\n",accessToken ,"\n", refreshToken)
    const loggedInUser = await User.findById(user._id).select("-password -refreshToken")

    // console.log(`this is loggedInUser Data  == ${loggedInUser} \n`)
    // console.log("access token = ", accessToken , "\n", "refreshToekn = ", refreshToken)
    const options = {
        httpOnly: true,
        secure: true
    }
    // console.log(`this is user._id == ${user._id}`)
    console.log("user loggedIn")


    return res
        .status(200)
        .cookie("accessToken", accessToken, options)
        .cookie("refreshToken", refreshToken, options)
        .json(
            new ApiResponse(200, user, "user logged in successfully")
        )


})

const logoutUser = asyncHandler(async (req, res) => {
    //remove cookies
    //remove refresh token
    //remove access token
    //find user by id
    // const user = User.findById(_id)
    // console.log(User.find({_id}))
    await User.findByIdAndUpdate(
        // console.log("this is id before new == true == ", user),
        req.user?._id,
        {
            $set: {
                refreshToken: 1,
            }
        },
        {
            new: true
        }
    )
    console.log("user loged out")
    // console.log(`this is _id after new == true == req.user._id === ${req.user._id}`)

    const options = {
        httpOnly: true,
        secure: true
    }
    return res.status(200)
        .clearCookie("accessToken", options)
        .clearCookie("refreshToken", options)
        .json(new ApiResponse(200, {}, "User logged Out"))
})

const refreshAccessToken = asyncHandler(async (req, res) => {
    //find refreshtoken token from cookies or body req.
    //verify accesstoken and refresh token

    const incommingRefreshToken = req.cookies.refreshToken || req.body.refreshToken
    // console.log(`this is incommingRefreshToken === ${incommingRefreshToken}`)

    if (!incommingRefreshToken) {
        throw new ApiError(401, "Unauthorized request")
    }

    try {
        const decodedToken = jwt.verify(incommingRefreshToken, process.env.REFRESH_TOKEN_SECRET)
        // console.log("this is decoded token for refreshing Access Token == ", decodedToken?._id)

        const user = await User.findById(
            decodedToken?._id
        )
        // console.log(`this is user._id == ${user._id}, ${user.fullname}, ${user.refreshToken}`)

        if (!user) {
            throw new ApiError(401, "invalid refresh token")
        }
        if (incommingRefreshToken !== user?.refreshToken) {
            throw new ApiError(401, "refreshed token expired or used")
        }
        const options = {
            httpOnly: true,
            secure: true
        }

        const { accessToken, newRefreshToken } = await generateAccessAndRefreshTokens(user._id)
        console.log(`new generated accesstoken and refreshtoken == ${accessToken} \n ${newRefreshToken}`)
        return res
            .status(200)
            .cookie("accessToken", accessToken, options)
            .cookie("refreshToken", newRefreshToken, options)
            .json(
                new ApiResponse(200, { accessToken, newRefreshToken }, "access token refreshed successfully")
            )
    } catch (error) {
        console.log("jwt error === ", error)
        throw new ApiError(401, error?.message || "Invalid refresh Token")
    }
})

const changeCurrentPassword = asyncHandler(async (req, res) => {
    const { oldPassword, newPassword } = req.body
    const id = req.user?._id;
    const user = await User.findById(id)
    // console.log(`this is req.user._id = ${user}`)
    console.log(`${user.fullname}`)

    const isPasswordCorrected = await user.isPasswordCorrect(oldPassword)
    console.log("is password correct == ", isPasswordCorrected)
    if (!isPasswordCorrected) {
        throw new ApiError(401, "invalid password")
    }
    user.password = newPassword
    await user.save({ validateBeforeSave: false })

    return res
        .status(200)
        .json(new ApiResponse(200, {}, "password changed successfully"))
})

const getCurrentUser = asyncHandler(async (req, res) => {
    console.log("current user == ", req.user?._id)
    return res
        .status(200)
        .json(200, req.user, "\ncurrent user fetched successfully")
})

const updateAccountDetails = asyncHandler(async (req, res) => {
    const { fullname, email, username } = req.body
    if (!(fullname || email || username)) {
        throw new ApiError(400, "All fields are required")
    }
    const user = await User.findByIdAndUpdate(
        req.user?._id,
        {
            $set: {
                fullname,
                username,
                email
            }
        },
        {
            new: true
        }
    ).select("-password")
    console.log("fullname === ", req.user?.fullname)
    return res.status(200)
        .json(new ApiResponse(200, user, "account updated successfully"))
})

//UpdateUser Image is under construction ........ 
const updateUserImages = asyncHandler(async (req, res) => {
    // fetch the images from req.body
    // return error if not coverImage || avatar 
    // verfiy the user from auth middleware
    // upload on cloudinary(public_id lai update gardini along with file ko path)
    const { coverImageLocalPath, avatarLocalPath } = req.files?.path

    if (!(coverImageLocalPath || avatarLocalPath)) {
        throw new ApiError(400, "CoverImage or Avatar Image required for updating an Image")
    }
    // const {avatar, coverImage} = await updateImages(req, res)

    const coverImage = await uploadOnCloudnary(coverImageLocalPath)
    const avatar = await uploadOnCloudnary(avatarLocalPath)

    // console.log(`this is the public_id of coverImage == ${cloud}`)
    if(!(coverImage.url || avatar.url)){
        throw new ApiError(400, "Error uploading avatar and coverImage")
    }
    
    

    const user = await User.findByIdAndUpdate(
        req.user?.id,
        {
            $set: {
                coverImage: coverImage.url,
                avatar: avatar.url
            }
        },
        {
            new: true
        }
    ).select("-password")

    return res
    .status(200)
    .json(new ApiResponse(200, user , "Image updated successfully"))
})
export {
    registerUser,
    loginUser,
    logoutUser,
    refreshAccessToken,
    changeCurrentPassword,
    getCurrentUser,
    updateAccountDetails,
    updateUserImages
};