import { v2 } from "cloudinary";
import { error } from "console";
import fs from "fs"


v2.config({
    cloud_name: process.env.CLOUD_NAME,
    api_key: process.env.API_KEY,
    api_secret: process.env.API_SECRET
});

const uploadOnCloudnary = async (localFilePath) => {
    try {
        if (!localFilePath)
            return error
        const response = await v2.uploader.upload(localFilePath, {
            resource_type: "auto"
        })
        console.log("file uploaded successfully --  ", response.url)
        fs.unlinkSync(localFilePath)
        console.log("Done uploading")
        // console.log(`this is public id == ${response.public_id}`)
        return response

    } catch (error) {
        fs.unlinkSync(localFilePath) // remove locally saved temp file if operation failed
        return null
    }
}

export {uploadOnCloudnary}