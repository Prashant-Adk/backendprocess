import mongoose, { Schema } from "mongoose";



const subscriptionSchema = new Schema({
    subscriber:{ // subscripting the other user chanel
        type: Schema.Types.ObjectId,
        ref: "User"
    },
    channel:{ //owner of the channel (user)
        type: Schema.Types.ObjectId, 
        ref: "User"
    },
    
},
{
    timestamps: true
})

export const subcription = mongoose.model("Subscription", subscriptionSchema)
