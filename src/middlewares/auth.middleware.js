import { User } from "../models/user.model.js";
import { ApiError } from "../utils/ApiError.js";
import { asyncHandler } from "../utils/asyncHandler.js";
import jwt from "jsonwebtoken"

export const verifyJWT = asyncHandler(async (req, _, next) => {
    try {
        const token = req.cookies?.accessToken || req.header("Authorization")?.replace("Bearer ", "")
        console.log(`token == ${token}`)
        if (!token) {
            throw new ApiError(401, "unauthorized request")
        }
        // console.log(`this is value of token == ${token}`)
    
        const decodedToken = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET)
        // console.log(`decode token from auth middleware = ${decodedToken._id}`)
        const user = await User.findById(decodedToken?._id).select("-password -refreshToken")
        // console.log(`this is user from auth middleware ${user}`)
        if(!user){
            throw new ApiError(401, "invalid access token")
        }
        req.user = user
        next()
    } catch (error) {
        throw new ApiError(401, error?.message || "Invalid access token")
    }

})