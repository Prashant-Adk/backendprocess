import mongoose from "mongoose";
import { DB_NAME } from "./constants.js";
import express from "express";
import connectDB from "./db/index.js";
import dotenv from 'dotenv'
import { app } from "./app.js";
dotenv.config({
    path: './env'
})

connectDB()
.then(()=> {
    app.listen(process.env.PORT || 8000, () => {
        console.log(`server started at http://127.0.0.1:${process.env.PORT}`)
    })
})
.catch((err) => {
    console.log("mongodb connection failed", err)
})











/*

const app = express()

(async () => { 
    try {
        await mongoose.connect(`${process.env.MONGODB_URL}/${DB_NAME}`)
        app.on("error", (error) => {
            console.log("error: ", error)
            throw error
        })

        app.listen(process.env.PORT, ()=> {
            console.log(`server conected on ${process.env.PORT}`)
        })
    } catch (error) {
        console.error("error: ------ ", error)
    }
}) ()
*/